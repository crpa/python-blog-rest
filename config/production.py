# -*- coding: UTF-8 -*-
import os
from .default import Config
basedir = os.path.abspath(os.path.dirname(__file__))


class ProductionConfig(Config):
    UPLOAD_FOLDER = '/usr/local/nginx/html/source'

