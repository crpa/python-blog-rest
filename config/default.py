# -*- coding: UTF-8 -*-
import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    WTF_CSRF_ENABLED = True
    SECRET_KEY = 'you-will-never-guess'

    # JSON_AS_ASCII = False
    ES_HOST = '10.0.97.122'
    ES_PORT = '9200'

    MONGODB_HOST = '127.0.0.1'
    MONGODB_PORT = 27017
    MONGODB_DB = 'cu'
    MONGODB_USERNAME = None
    MONGODB_PASSWORD = None

    UPLOAD_FOLDER = 'D:\\temp'
    PREFIX_ATTACHED = 'source/'
