import os
# from config import basedir
from flask import Flask
from config import load_config  # 绝对导入
# from flask_sqlalchemy import SQLAlchemy
# from flask_restful import reqparse, abort, Api, Resource
app = Flask(__name__)
# api = Api(app)
# app.config.from_object('config')
config = load_config()
app.config.from_object(config)
from app.site import wikis
from app.user_manager import user_manager
from app.make_number import make_number
app.register_blueprint(wikis, url_prefix='/wiki')
app.register_blueprint(user_manager, url_prefix='/user')
app.register_blueprint(make_number, url_prefix='/mn')
