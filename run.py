#!/usr/bin/python
from app import app

def main():
    # app.run(debug=True)
    # initconfig.start_init()
    # initconfig_redis.start_init()
    app.run(host="0.0.0.0", debug=False)


if __name__ == '__main__':
    main()
